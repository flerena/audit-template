import { ethers } from "hardhat";
import dotenv from 'dotenv';


var ContractName  = "Lock";


async function main() {
  // example args
  const currentTimestampInSeconds = Math.round(Date.now() / 1000);
  const unlockTime = currentTimestampInSeconds + 60;
  const lockedAmount = ethers.utils.parseEther("0.001");


  const Contract = await ethers.getContractFactory(ContractName);
  const contract = await Contract.deploy(unlockTime, { value: lockedAmount });

  await contract.deployed();
  console.log(`${ContractName} deployed to ${contract.address}`);


  // example output dependant on contract
  console.log(`Lock with ${ethers.utils.formatEther(lockedAmount)}ETH and unlock timestamp ${ unlockTime } deployed to ${contract.address}`);
}

// Recommended to use async/await everywhere

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});


